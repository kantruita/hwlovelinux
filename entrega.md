# Extract hardware info from host with linux

## system

### system model, bios version and bios date, how old is the hardware?
Per trobar el model del sistema utilitzem la comanda dmidecode amb la opció -t,  la keyword system i la opció --version 
```
[root@a12 ~]# dmidecode -t system --version
3.2
```
Per trobar la versió de la BIOS podem utilitzar la comanda dmidecode amb la opció -s i la keyword bios-version
```
[root@a12 ~]# dmidecode -s bios-version
F5
```
I per trobar la data amb la comanda dmidecode amb la opció -s i la keyword bios-release-date
```
[root@a12 ~]# dmidecode -s bios-release-date
01/17/2014
```
## mainboard
### mainboard model, link to manual, link to product
per trobar el model de la placa base utilitzem la comanda dmidecode amb la opció -s i la keyword baseboard-product-name
```
[root@a12 ~]# dmidecode -s baseboard-product-name
H81M-S2PV
```
I aqui hi ha:
link al manual:http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#support-manual 
link al producte:http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#kf 

### memory banks (free or occupied)

### how many disks and types can be connected

### chipset, link to 

## cpu

### cpu model, year, cores, threads, cache 
Per trobar el model de la CPU amb la comanda lscpu 
Model name:                      Intel(R) Pentium(R) CPU G3258 @ 3.20GHz

L'any de la CPU:


Cores :
Core(s) per socket:              2

threads:
Thread(s) per core:              1



### socket 

## pci

### number of pci slots, lanes available
 
### devices connected

### network device, model, kernel module, speed

### audio device, model, kernel module

### vga device, model, kernel module

## hard disks

### /dev/* , model, bus type, bus speed

### test fio random (IOPS) and sequential (MBps)
## per quan vas tard
Per si vas tard, amb el comando

```
history
```
et mostrarà l'historial de totes les comandes que has fet al dia.


## sept 21

## nov 11

##ordre lshw

La lshw ens informa, per poder mostrar informació en detall fem servir: 

PROCEDIMENT
    Primer, cal descobrir sobre la comanda
```
lshw -- help 
man lshw 
```
    en aquest cas hem vist com aquestes comandes
```
lshw -businfo 
lshw -short
```
    també ens ajudan per arribar a trobar la informació nessesària

COMANDA FINAL 
    Aquesta és la comanda que ens mostrarà la informació específica
```
lshw -class
```
## dmidecode

Aquesta comanda ens mostra la info de la ram

```
dmidecode -t 
```

## 1 desembre

# Sistemes de fitxers 
El sistema més antic es el fat16 però tenia limitació de tamany 
després el sistema fat32 treu la limitació de tamany 

fat16
--> limitació tamany màxim de partició

fat32 
--> limitació de tamany màxim d'arxiu a 4G

exFAT 
--> no hi ha limits
--> pensat per transferir fitxers amb pendrives sense control d'usuaris molt estrices
